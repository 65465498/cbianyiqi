package compiler.my_parser;

/**
 * Created by szj on 2017/5/30.
 */
public enum SymbolDefinition {
    STATEMENT,
    EXPR,
    TERM,
    FACTOR,

    PLUS,
    TIMES,
    NUM_OR_ID,
    LEFT_PAREN,
    RIGHT_PAREN,
    EOF,
    UNKNOWN;

    public static String getName(int value) {
        return SymbolDefinition.values()[value].toString();
    }

    public static boolean isTerminal(int value) {
        return value > FACTOR.ordinal() && value <= EOF.ordinal();
    }
}
