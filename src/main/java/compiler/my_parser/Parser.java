package compiler.my_parser;

/**
 * Created by szj on 2017/5/30.
 */
public class Parser {

    public static void main(String[] arg0) {
        ProductionManager.getInstance()
                .initProductions()
                .printProductions()
                .doFirstSet();
        StateManger.getInstance()
                .buildStateMachine();
    }
}
