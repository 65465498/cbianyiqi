package compiler.my_parser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by szj on 2017/5/30.
 */
public class Symbols {
    public int value;
    public boolean isNullable;
    public List<int[]> rightPortion;
    public List<Integer> firstSet = new ArrayList<>();

    public Symbols(int value, boolean nullable, List<int[]> rightPortion) {
        this.value = value;
        this.rightPortion = rightPortion;
        isNullable = nullable;

        if (SymbolDefinition.isTerminal(value)) {
            firstSet.add(value);
        }
    }

    public void addProduction(int[] production) {
        if (!rightPortion.contains(production)) {
            rightPortion.add(production);
        }
    }
}
