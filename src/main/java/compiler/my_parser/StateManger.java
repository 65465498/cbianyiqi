package compiler.my_parser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by szj on 2017/5/30.
 */
public class StateManger {

    private static StateManger instance;
    private static boolean transitionDone = false;
    private List<State> stateList = new ArrayList<>();

    public static StateManger getInstance() {
        if (instance == null) {
            instance = new StateManger();
        }
        return instance;
    }

    public void buildStateMachine() {
        ProductionManager productionManager = ProductionManager.getInstance();
        List<Production> production = productionManager.getProduction(SymbolDefinition.STATEMENT.ordinal());
        State state = createState(production);
        state.createTransition();
    }

    public State createState(List<Production> productionList) {
        State state = new State(productionList);
        if (!stateList.contains(state)) {
            stateList.add(state);
            State.increaseStateNum();
            return state;
        }

        for (State aStateList : stateList) {
            if (aStateList.equals(state)) {
                return aStateList;
            }
        }

        return null;
    }

    private State getState(List<Production> productionList) {
        State state = new State(productionList);
        int index = stateList.indexOf(state);

        if (index == -1) {
            stateList.add(state);
            State.increaseStateNum();
            return state;
        }

        return stateList.get(index);
    }
}
