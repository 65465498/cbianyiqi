package compiler._dfa;

import compiler._nfa.Nfa;

import java.util.HashSet;
import java.util.Set;

public class Dfa {
    private static int STATE_NUM = 0;
    public Set<Nfa> nfaStates = new HashSet<>();
    int stateNum = 0;
    boolean accepted = false;

    public static Dfa newDfa(Set<Nfa> input) {
        Dfa dfa = new Dfa();

        for (Nfa nfa : input) {
            dfa.nfaStates.add(nfa);
            if (nfa.next == null && nfa.next2 == null) {
                dfa.accepted = true;
            }
        }

        dfa.stateNum = STATE_NUM;
        STATE_NUM++;
        return dfa;
    }


    public boolean hasNfaStates(Set<Nfa> set) {
        if (this.nfaStates.equals(set)) {
            return true;
        }

        return false;
    }
}
