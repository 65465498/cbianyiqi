package compiler.tiny.enums;

/**
 * Created by 孙证杰 on 2017/5/19.
 */
public enum TokenType {
    IF,
    THEN,
    ELSE,
    PLUS,
    MINUS,
    NUM,
    ID
}
