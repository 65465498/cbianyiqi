package compiler.enums;

/**
 * Created by szj on 2017/5/7.
 */
public enum Token {
    EOS, //正则表达式末尾
    ANY,     // . 通配符
    AT_BOL,  //^ 开头匹配符
    AT_EOL,  //$ 末尾匹配符
    CCL_END,  //字符集类结尾括号 ]
    CCL_START,  //字符集类开始括号 [
    CLOSE_CURLY, // }
    CLOSE_PAREN,  //)
    CLOSURE,      //*
    DASH,       // -
    END_OF_INPUT,  //输入流结束
    L,        //字符常量
    OPEN_CURLY, // {
    OPEN_PAREN, // (
    OPTIONAL,  //?
    OR,       // |
    PLUS_CLOSE
}
