package compiler.input;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;
import java.util.Objects;
import java.util.Scanner;

/**
 * Created by szj on 2017/5/7.
 */
public class Input {
    public static final int EOF = -1;
    private PushbackReader reader;
    private Scanner scanner = new Scanner(System.in);

    public static boolean isSkipChar(int character) {
        return Character.isSpaceChar(character)
                || character == '\n';
    }

    public void readInput() {
        StringBuilder builder = new StringBuilder();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (Objects.equals(line, "end")) {
                break;
            }
            if (Objects.equals(line.trim(), "")) {
                continue;
            }
            builder.append(line);
        }
        this.reader = new PushbackReader(new StringReader(builder.toString()));
    }

    public void closeInput() {
        scanner.close();
    }

    public int lookAhead() {
        int character;
        try {
            character = reader.read();
            if (character != -1) {
                reader.unread(character);
            }
        } catch (IOException e) {
            return -1;
        }
        return character;
    }

    public int advance() {
        try {
            return reader.read();
        } catch (IOException e) {
            return -1;
        }
    }
}
