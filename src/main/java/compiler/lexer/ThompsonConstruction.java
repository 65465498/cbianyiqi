package compiler.lexer;

import compiler.enums.Token;
import compiler.input.Input;

/**
 * Created by szj on 2017/5/7.
 */
public class ThompsonConstruction {
    private Input input = new Input();
    private MacroHandler macroHandler = null;
    private RegularExpressionHandler regularExpr = null;
    private Lexer lexer = null;

    public void runLexerExample() {

        macroHandler = new MacroHandler(input);
        regularExpr = new RegularExpressionHandler(input, macroHandler);
        lexer = new Lexer(regularExpr);

        int exprCount = 0;
        System.out.println("当前正则解析的正则表达式: " + regularExpr.getRegularExpression(exprCount));

        lexer.advance();

        while (!lexer.MatchToken(Token.END_OF_INPUT)) {

            if (!lexer.MatchToken(Token.EOS)) {
//                printLexResult();
                continue;
            }

            exprCount++;
            String reg = regularExpr.getRegularExpression(exprCount);
            if (reg != null) {
                System.out.println("解析下一个正则表达式");
                System.out.println("当前正则解析的正则表达式: ");
            }

            lexer.advance();
        }
    }
}
