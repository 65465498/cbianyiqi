package compiler;

import compiler._dfa.DfaConstructor;
import compiler._dfa.MinimizeDFA;
import compiler._nfa.NfaInterpreter;
import compiler._nfa.NfaMachineConstructor;
import compiler._nfa.NfaPair;
import compiler._nfa.NfaPrinter;
import compiler.input.Input;
import compiler.lexer.Lexer;
import compiler.lexer.MacroHandler;
import compiler.lexer.RegularExpressionHandler;

/**
 * Created by 孙证杰 on 2017/5/11.
 */
public class DfaTest {

    public static void main(String[] arg0){
        Input input = new Input();
        MacroHandler macroHandler = new MacroHandler(input);
        RegularExpressionHandler regularExpr = new RegularExpressionHandler(input, macroHandler);
        Lexer lexer = new Lexer(regularExpr);
        NfaMachineConstructor constructor = new NfaMachineConstructor(lexer);
        NfaPair nfaPair = new NfaPair();
        constructor.expr(nfaPair);
        NfaPrinter printer = new NfaPrinter();
        printer.printNfa(nfaPair.startNode);
        NfaInterpreter nfaInterpreter = new NfaInterpreter(nfaPair.startNode, input);
        nfaInterpreter.interpretNfa();
        DfaConstructor dfaConstructor = new DfaConstructor(nfaPair, nfaInterpreter);
        dfaConstructor.convertNfaToDfa();
        MinimizeDFA minimizeDFA = new MinimizeDFA(dfaConstructor);
        minimizeDFA.minimize();
    }
}
